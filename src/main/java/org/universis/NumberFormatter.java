package org.universis;
import com.fasterxml.jackson.databind.ObjectMapper;
// import sun.misc.Regexp;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberFormatter {

    private static final Map<String,NumberFormatterLocale> locales = new HashMap<String, NumberFormatterLocale>() {{
        try {
            InputStream inputStream = NumberFormatterLocale.class.getResource("el.json").openStream();
            put("el", new ObjectMapper().readValue(inputStream, NumberFormatterLocale.class));
            inputStream = NumberFormatterLocale.class.getResource("en.json").openStream();
            put("en", new ObjectMapper().readValue(inputStream, NumberFormatterLocale.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }};

    public static NumberFormatter registerLocale(String locale, NumberFormatterLocale data) {
        NumberFormatter.locales.put(locale, data);
        return new NumberFormatter();
    }

    protected String formatIntegerPart(float number, String locale) {
        // format number
        NumberFormatterLocale currentLocale = NumberFormatter.locales.get(locale);
        String result = "";
        float nextValue = number;
        Object[] keys = currentLocale.values.keySet().toArray();
        // if nextValue is zero return zero string
        if (nextValue == 0) {
            LocaleValue findZero = currentLocale.values.get("0");
            if (findZero != null) {
                result = findZero.one;
            }
            return result;

        }
        // enumerate formatter keys
        for (int index = 0; index < keys.length; index++) {
            String key = (String)keys[index];
            // convert property value to number
            float divider = Float.parseFloat(key);
            // divide object
            double nextResult = Math.floor(nextValue / divider);
            if (nextResult >= 1) {
                if (nextResult > 1) {
                    result += " ";
                    result += this.formatIntegerPart((float) nextResult, locale);
                }
                LocaleValue value = currentLocale.values.get(key);
                if (value != null) {
                    result += " ";
                    if (nextResult == 1 && nextValue > divider && value.more != null) {
                        result += value.more;
                    }
                    else {
                        result += nextResult == 1 ? value.one : value.many;
                    }
                } else {
                    throw new Error("Invalid number formatter provider value.");
                }
            }
            // get next value
            nextValue = nextValue % divider;
            // if value is zero break and exit
            if (nextValue == 0) {
                break;
            }
            // otherwise continue
        }
        // trim result
        return result.trim();
    }

    protected String formatFractionalPart(float number, String locale, int fractionDigits) {

        if (number < 0) {
            throw new Error("Franctional part of a number cannot be less than zero");
        }
        // if number is zero
        if (number == 0) {
            // do nothing
            return "";
        }
        NumberFormatterLocale currentLocale = NumberFormatter.locales.get(locale);
        // get values lower than 1
        Object[] keys = currentLocale.values.keySet().toArray();
        float[] values = new float[keys.length];
        for (int i = 0; i < keys.length; i++) {
            values[i] = Float.parseFloat((String)keys[i]);
        }
        String result = "";
        // enumerate locale values
        for (int i = 0; i < values.length; i++) {
            float value = values[i];
            // test value that it is lower than 1 e.g. 0.01
            if (value > 0 && value < 1) {
                String valueText = (String)keys[i];
                // if result is greater or equal to 1
                if (valueText.length() - 2 == fractionDigits) {
                    // get key value
                    LocaleValue keyValue = currentLocale.values.get((String)keys[i]);
                    // format number e.g. 45 => a cardinal number
                    result = this.formatIntegerPart(number, locale);
                    result += " ";
                    // and get key value description
                    result += number == 1 ? keyValue.one : keyValue.many;
                }
            }
        }
        return result;
    }

    public String format(float number, String locale) {
        return this.format(number, locale, -1);
    }


    public String format(float number, String locale, int fractionDigits) {
        if (!NumberFormatter.locales.containsKey(locale)) {
            throw new Error("The specified locale is missing for registered locales.");
        }
        // format number
        NumberFormatterLocale currentLocale = NumberFormatter.locales.get(locale);
        // get integer part
        DecimalFormat df = new DecimalFormat();
        df.setGroupingUsed(false);
        if (fractionDigits >= 0) {
            df.setMaximumFractionDigits(fractionDigits);
            df.setMinimumFractionDigits(fractionDigits);
        }
        String numberString = df.format(number);
        char decimalSeparator = df.getDecimalFormatSymbols().getDecimalSeparator();
        Pattern integerPattern = Pattern.compile("^(\\d+)\\" + decimalSeparator + "?");
        Matcher integerMatch = integerPattern.matcher(numberString);
        int integerPart = 0;
        if (integerMatch.find()) {
            integerPart = Integer.parseInt(integerMatch.group(1));
        }
        // format integer part
        String result = this.formatIntegerPart(integerPart, locale);
        // get fractional part
        Pattern fractionalPattern = Pattern.compile("\\" + decimalSeparator + "(\\d+)$");
        Matcher fractionalMatch = fractionalPattern.matcher(numberString);
        // if fractional part exists
        if (fractionalMatch.find()) {
            // convert group(0) to a valid number
            String group0 = fractionalMatch.group(0).replace(decimalSeparator, '.');
            int fractionalPart = Math.round(Float.parseFloat(group0) * (int)Math.pow(10, fractionalMatch.group(1).length()));
            String fractionalResult = this.formatFractionalPart(fractionalPart, locale, fractionDigits > 0 ? fractionDigits : fractionalMatch.group(1).length() );
            if (fractionalResult != null && fractionalResult.length() > 0) {
                result += " ";
                if (currentLocale.decimalSeparator != null && currentLocale.decimalSeparator.length() > 0) {
                    result += currentLocale.decimalSeparator;
                    result += " ";
                }
                result += fractionalResult;
            }
        }
        result = currentLocale.spellcheck(result);
        return result;
    }

    public String format(double number, String locale) {
        return this.format((float) number, locale, -1);
    }

    public String format(double number, String locale, int fractionDigits) {
        return this.format((float) number, locale, fractionDigits);
    }

    public String format(int number, String locale) {
        return this.format((float) number, locale, -1);
    }

    public String format(short number, String locale) {
        return this.format(number, locale, -1);
    }

    public String format(float number, String locale, int fractionDigits, boolean upperCase) {
        String result = this.format(number, locale, fractionDigits);
        String upper = result.toUpperCase();
        String temp = Normalizer.normalize(upper, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }

    public String format(float number, String locale, boolean upperCase) {
        return this.format(number, locale, -1, upperCase);
    }

    public String format(double number, String locale, int fractionDigits, boolean upperCase) {
        return this.format((float) number, locale, fractionDigits, upperCase);
    }

    public String format(int number, String locale, int fractionDigits, boolean upperCase) {
        return this.format((float) number, locale, fractionDigits, upperCase);
    }

    public String format(double number, String locale, boolean upperCase) {
        return this.format((float)number, locale, -1, upperCase);
    }

}
