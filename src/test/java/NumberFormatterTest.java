import com.fasterxml.jackson.databind.ObjectMapper;
import org.universis.LocaleValue;
import org.universis.NumberFormatter;
import org.universis.NumberFormatterLocale;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class NumberFormatterTest {
    @org.junit.jupiter.api.Test
    void createClass() {
        NumberFormatter formatter = new NumberFormatter();
        assertNotNull(formatter);
    }

    @org.junit.jupiter.api.Test
    void testParseNumberFormatterLocale() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        InputStream inStream = NumberFormatterLocale.class.getResource("el.json").openStream();
        NumberFormatterLocale formatter = mapper.readValue(inStream, NumberFormatterLocale.class);
        assertNotNull(formatter);
    }

    @org.junit.jupiter.api.Test
    void testFormatFloat() throws IOException {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        char decimalSeparator = df.getDecimalFormatSymbols().getDecimalSeparator();
        String value = String.format("%.02f", 0.456);
        assertEquals(value, "0" + decimalSeparator + "46");
        assertEquals(df.format(0.456), "0" + decimalSeparator + "46");
        assertEquals(df.format(0.4), "0" + decimalSeparator + "40");
    }

    @org.junit.jupiter.api.Test
    void testIntegers() throws IOException {
        NumberFormatter formatter = new NumberFormatter();
        assertEquals(formatter.format(0, "el"),"μηδέν");
        assertEquals(formatter.format(1010342, "el"),"ένα εκατομμύριο δέκα χιλιάδες τριακόσια σαράντα δύο");
        assertEquals(formatter.format(102, "el"),"εκατόν δύο");
        assertEquals(formatter.format(100, "el"),"εκατό");
        assertEquals(formatter.format(950, "el"),"εννιακόσια πενήντα");
        assertEquals(formatter.format(1745, "el"),"χίλια επτακόσια σαράντα πέντε");
        assertEquals(formatter.format(10342, "el"),"δέκα χιλιάδες τριακόσια σαράντα δύο");
        assertEquals(formatter.format(120342, "el"),"εκατόν είκοσι χιλιάδες τριακόσια σαράντα δύο");
    }

    @org.junit.jupiter.api.Test
    void testDecimals() throws IOException {
        NumberFormatter formatter = new NumberFormatter();
        assertEquals(formatter.format(0, "el"),"μηδέν");
        assertEquals(formatter.format((float) 0.4, "el"),"μηδέν και τέσσερα δέκατα");
        assertEquals(formatter.format((float)1002.7, "el"),"χίλια δύο και επτά δέκατα");
        assertEquals(formatter.format(6.75, "en"),"six and seventy-five hundredths");
        assertEquals(formatter.format(9.75, "en", 3),"nine and seven hundred fifty thousandths");
        assertEquals(formatter.format(13.450, "en", 3),"thirteen and four hundred fifty thousandths");
        assertEquals(formatter.format(1016.104, "en", 3),"one thousand sixteen and one hundred four thousandths");
    }

    @org.junit.jupiter.api.Test
    void testUpperCase() {
        NumberFormatter formatter = new NumberFormatter();
        String res = formatter.format(0.4, "el", true);
        assertEquals(res, "ΜΗΔΕΝ ΚΑΙ ΤΕΣΣΕΡΑ ΔΕΚΑΤΑ");
        res = formatter.format(14350, "en", true);
        assertEquals(res, "FOURTEEN THOUSAND THREE HUNDRED FIFTY");
        res = formatter.format(6.75, "en", true);
        assertEquals(res, "SIX AND SEVENTY-FIVE HUNDREDTHS");
    }

    @org.junit.jupiter.api.Test
    void testSpecificFractionalPart() {
        NumberFormatter formatter = new NumberFormatter();
        String res = formatter.format(0.1, "el", 1);
        assertEquals(res, "μηδέν και ένα δέκατο");
        res = formatter.format(0.01, "el", 2);
        assertEquals(res, "μηδέν και ένα εκατοστό");
        res = formatter.format(0.001, "el", 3);
        assertEquals(res, "μηδέν και ένα χιλιοστό");
        res = formatter.format(7.1, "el", 1);
        assertEquals(res, "επτά και ένα δέκατο");
        res = formatter.format(7.01, "el", 2);
        assertEquals(res, "επτά και ένα εκατοστό");
        res = formatter.format(7.001, "el", 3);
        assertEquals(res, "επτά και ένα χιλιοστό");
    }
}